package com.example.webservicesample;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Xml;

public class XMLParser {
	private static final String ns = null;
	XmlPullParser parser;
	InputStream in = null;
	List<Row> rowList;

	public XMLParser(InputStream in) throws XmlPullParserException, IOException {
		this.in = in;
		rowList = new ArrayList<Row>();
		parser = Xml.newPullParser();
		parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
		parser.setInput(in, null);
		parser.nextTag();
	}

	List<Row> readMedalTally() throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "MedalTally");
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			if (parser.getName().equals("Row"))
				rowList.add(readRow());
			else
				skip();

		}
		return rowList;
	}

	Row readRow() throws XmlPullParserException, IOException {

		Row row = new Row();
		parser.require(XmlPullParser.START_TAG, ns, "Row");
		parser.nextTag();
		row.setStateName(readStateName());
		parser.nextTag();
		row.setGold(readGoldCount());
		parser.nextTag();
		row.setSilver(readSilverCount());
		parser.nextTag();
		row.setBronze(readBronzeCount());
		parser.nextTag();
		row.setTotal(readTotal());
		parser.nextTag();
		parser.require(XmlPullParser.END_TAG, ns, "Row");
		return row;

	}

	String readStateName() throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "StateName");
		String name = readText();
		parser.require(XmlPullParser.END_TAG, ns, "StateName");
		return name;
	}

	int readGoldCount() throws XmlPullParserException, IOException {

		parser.require(XmlPullParser.START_TAG, ns, "Gold");
		int count = Integer.parseInt(readText());
		parser.require(XmlPullParser.END_TAG, ns, "Gold");
		return count;
	}

	int readSilverCount() throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "Silver");
		int count = Integer.parseInt(readText());
		parser.require(XmlPullParser.END_TAG, ns, "Silver");
		return count;

	}

	int readBronzeCount() throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "Bronze");
		int count = Integer.parseInt(readText());
		parser.require(XmlPullParser.END_TAG, ns, "Bronze");
		return count;

	}

	int readTotal() throws XmlPullParserException, IOException {

		parser.require(XmlPullParser.START_TAG, ns, "Total");
		int total = Integer.parseInt(readText());
		parser.require(XmlPullParser.END_TAG, ns, "Total");
		return total;
	}

	// For the tags title and summary, extracts their text values.
	private String readText() throws IOException, XmlPullParserException {
		String result = "";
		if (parser.next() == XmlPullParser.TEXT) {
			result = parser.getText();
			parser.nextTag();
		}
		return result;
	}

	private void skip() throws XmlPullParserException, IOException {
		if (parser.getEventType() != XmlPullParser.START_TAG) {
			throw new IllegalStateException();
		}
		int depth = 1;
		while (depth != 0) {
			switch (parser.next()) {
			case XmlPullParser.END_TAG:
				depth--;
				break;
			case XmlPullParser.START_TAG:
				depth++;
				break;
			}
		}
	}

	void close() {

		try {
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block

		}

	}

}
